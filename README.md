# README #

This is a Pong game made entirely using [Processing 3](https://processing.org/download/)

### What is this repository for? ###

* This is an excercise designed to learn Design Patterns.
* The programmer is encouraged to submit pull requests with some part of the code refactored to design patterns or to create new features using design patterns. 

### How do I get set up? ###

* Download and install [Processing 3](https://processing.org/download/)
* Open the project using Processing IDE

### Contribution guidelines ###

* Refactoring to patterns pull requests accepted
* Code review and code stlye pull requests accepted
* Please use processing IDE when making submissions
